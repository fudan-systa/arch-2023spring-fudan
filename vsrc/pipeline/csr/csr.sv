`ifndef __CSR_SV
`define __CSR_SV


module csr
	import common::*;
	import decode_pkg::*;
	import csr_pkg::*;(
	input logic clk, reset, valid, is_mret,
	input u12 self_ra, self_wa,
	input u64 self_wd,
	output u64 self_rd,
	output u64 pcselect

);
	csr_regs_t regs, regs_nxt;

	always_ff @(posedge clk) begin
		if (reset) begin
			regs <= '0;
			regs.mcause[1] <= 1'b1;
			regs.mepc[31] <= 1'b1;
		end else begin
			regs <= regs_nxt;
		end
	end

	// read
	always_comb begin
		self_rd = '0;
		unique case(self_ra)
			CSR_MIE: self_rd = regs.mie;
			CSR_MIP: self_rd = regs.mip;
			CSR_MTVEC: self_rd = regs.mtvec;
			CSR_MSTATUS: self_rd = regs.mstatus;
			CSR_MSCRATCH: self_rd = regs.mscratch;
			CSR_MEPC: self_rd = regs.mepc;
			CSR_MCAUSE: self_rd = regs.mcause;
			CSR_MCYCLE: self_rd = regs.mcycle;
			CSR_MTVAL: self_rd = regs.mtval;
			default: begin
				self_rd = '0;
			end
		endcase
	end

	// write
	always_comb begin
		regs_nxt = regs;
		regs_nxt.mcycle = regs.mcycle + 1;
		// Writeback: W stage
		unique if (valid) begin
			unique case(self_wa)
				CSR_MIE: regs_nxt.mie = self_wd;
				CSR_MIP:  regs_nxt.mip = self_wd;
				CSR_MTVEC: regs_nxt.mtvec = self_wd;
				CSR_MSTATUS: regs_nxt.mstatus = self_wd;
				CSR_MSCRATCH: regs_nxt.mscratch = self_wd;
				CSR_MEPC: regs_nxt.mepc = self_wd;
				CSR_MCAUSE: regs_nxt.mcause = self_wd;
				CSR_MCYCLE: regs_nxt.mcycle = self_wd;
				CSR_MTVAL: regs_nxt.mtval = self_wd;
				default: begin
					
				end
				
			endcase
			regs_nxt.mstatus.sd = regs_nxt.mstatus.fs != 0;
		end else if (is_mret) begin
			regs_nxt.mstatus.mie = regs_nxt.mstatus.mpie;
			regs_nxt.mstatus.mpie = 1'b1;
			regs_nxt.mstatus.mpp = 2'b0;
			regs_nxt.mstatus.xs = 0;
		end
		else begin end
	end
	assign pcselect.mepc = regs.mepc;
	
	
endmodule

`endif